package com.nbarthelemy.paiementbdm.api.repository;

import com.nbarthelemy.paiementbdm.api.entities.EventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EventRepository extends JpaRepository<EventEntity, Integer> {
    @Query("SELECT 1 FROM EventEntity ")
	int refreshDB();
    List<EventEntity> findAllBySeasonId(int seasonId);
}
