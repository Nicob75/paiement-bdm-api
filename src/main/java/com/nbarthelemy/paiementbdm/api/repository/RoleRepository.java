package com.nbarthelemy.paiementbdm.api.repository;

import com.nbarthelemy.paiementbdm.api.entities.ERole;
import com.nbarthelemy.paiementbdm.api.entities.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    Optional<RoleEntity> findByName(ERole name);
}
