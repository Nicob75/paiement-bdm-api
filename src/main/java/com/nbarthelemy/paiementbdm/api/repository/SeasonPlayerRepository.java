package com.nbarthelemy.paiementbdm.api.repository;

import com.nbarthelemy.paiementbdm.api.entities.SeasonPlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SeasonPlayerRepository extends JpaRepository<SeasonPlayerEntity, Integer> {
	List<SeasonPlayerEntity> findAllBySeasonId(int seasonId);
	List<SeasonPlayerEntity> findAllByPlayerId(int playerId);
	SeasonPlayerEntity findBySeasonIdAndPlayerId(int seasonId, int playerId);
	void deleteBySeasonIdAndPlayerId(int seasonId, int playerId);
}
