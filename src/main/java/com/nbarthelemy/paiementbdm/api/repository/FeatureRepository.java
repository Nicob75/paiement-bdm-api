package com.nbarthelemy.paiementbdm.api.repository;

import com.nbarthelemy.paiementbdm.api.entities.FeatureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FeatureRepository extends JpaRepository<FeatureEntity, Integer> {
    @Query("SELECT f FROM FeatureEntity f WHERE f.id NOT IN (SELECT u.featureId FROM UserReadFeatureEntity u WHERE u.userId = :userId)")
    List<FeatureEntity> getUserUnreadFeatures(@Param("userId") long userId);
}
