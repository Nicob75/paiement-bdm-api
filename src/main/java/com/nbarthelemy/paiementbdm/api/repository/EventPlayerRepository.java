package com.nbarthelemy.paiementbdm.api.repository;

import com.nbarthelemy.paiementbdm.api.entities.EventPlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface EventPlayerRepository extends JpaRepository<EventPlayerEntity, Integer> {
	List<EventPlayerEntity> findAllByEventId(int eventId);
	List<EventPlayerEntity> findAllByPlayerId(int playerId);
	Optional<EventPlayerEntity> findByEventIdAndPlayerId(int eventId, int playerId);
}
