package com.nbarthelemy.paiementbdm.api.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated()
@Getter
@Setter
public class SubmitEvent {
    private Event event;
    private List<EventPlayer> eventPlayerList;
}
