package com.nbarthelemy.paiementbdm.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode( callSuper = true )
@Data
public class Player extends Auditable<String> {
	@Schema( accessMode = Schema.AccessMode.READ_ONLY )
	private int id;
	private int bdmId;
	private String firstname;
	private String lastname;
	private int jerseyNumber;
	private List<Season> seasons;
}
