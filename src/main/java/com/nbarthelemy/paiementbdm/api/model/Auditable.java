package com.nbarthelemy.paiementbdm.api.model;

import lombok.Data;

import java.util.Date;

@Data
public abstract class Auditable<U> {
    protected U createdBy;
    protected Date createdDate;
    protected U lastModifiedBy;
    protected Date lastModifiedDate;
}
