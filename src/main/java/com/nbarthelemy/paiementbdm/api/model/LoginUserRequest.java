package com.nbarthelemy.paiementbdm.api.model;

import lombok.Data;

@Data
public class LoginUserRequest {
    String userName;
    String password;
}
