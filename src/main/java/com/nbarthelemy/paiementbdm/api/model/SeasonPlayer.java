package com.nbarthelemy.paiementbdm.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode( callSuper = true )
@Data
public class SeasonPlayer extends Auditable<String> {
	@Schema( accessMode = Schema.AccessMode.READ_ONLY )
	private int id;
	private int seasonId;
	private int playerId;
}
