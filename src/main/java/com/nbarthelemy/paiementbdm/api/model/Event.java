package com.nbarthelemy.paiementbdm.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@EqualsAndHashCode( callSuper = true )
@Data
public class Event extends Auditable<String> {
	@Schema( accessMode = Schema.AccessMode.READ_ONLY )
	private int id;
	private String homeTeam;
	private String homeScore;
	private String visitorScore;
	private String visitorTeam;
	private Date matchDate;
	private int matchDay;
	private String playCategory;
	private String division;
	private int totalPlayers;
	private float totalPayment;
	private float totalKeeper;
	private int seasonId;
	private boolean totalPlayersValid;
	private boolean totalPaymentValid;
	private boolean totalKeeperValid;
}
