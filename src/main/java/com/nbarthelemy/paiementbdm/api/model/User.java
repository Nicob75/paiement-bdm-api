package com.nbarthelemy.paiementbdm.api.model;

import com.nbarthelemy.paiementbdm.api.entities.ERole;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class User {
	@Schema( accessMode = Schema.AccessMode.READ_ONLY )
	private int id;
	private String email;
	private String username;
	private boolean passwordExpired;
	private Date lastLoginDate;
	private List<ERole> roles;
}
