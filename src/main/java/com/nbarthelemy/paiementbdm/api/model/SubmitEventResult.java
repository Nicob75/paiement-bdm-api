package com.nbarthelemy.paiementbdm.api.model;

import com.nbarthelemy.paiementbdm.api.common.StatusEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class SubmitEventResult {
    private Event event;
    private Map<EventPlayer, StatusEnum> processedEventPlayers;
    private List<EventPlayer> resultEventPlayerList;
}
