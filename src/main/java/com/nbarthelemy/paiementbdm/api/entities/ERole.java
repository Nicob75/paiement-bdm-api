package com.nbarthelemy.paiementbdm.api.entities;

public enum ERole {
    ROLE_USER,
    ROLE_MANAGER,
    ROLE_ADMIN
}
