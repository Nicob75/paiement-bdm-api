package com.nbarthelemy.paiementbdm.api.entities;

import lombok.*;

import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "userreadfeatures")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserReadFeatureEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="userid")
	private long userId;
	@Column(name="featureid")
	private int featureId;
}
