package com.nbarthelemy.paiementbdm.api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import jakarta.persistence.*;
import java.sql.Timestamp;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "events")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class EventEntity extends AuditableEntity<String> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="hometeam")
	private String homeTeam;
	@Column(name="homescore")
	private String homeScore;
	@Column(name="visitorscore")
	private String visitorScore;
	@Column(name="visitorteam")
	private String visitorTeam;
	@Column(name="matchdate")
	private Timestamp matchDate;
	@Column(name="matchday")
	private int matchDay;
	@Column(name="playcategory")
	private String playCategory;
	@Column(name="division")
	private String division;
	@Column(name="totalplayers")
	private int totalPlayers;
	@Column(name="totalpayment")
	private float totalPayment;
	@Column(name="totalkeeper")
	private float totalKeeper;
	@Column(name="seasonid")
	private int seasonId;

	public EventEntity(String homeTeam, String homeScore, String visitorScore, String visitorTeam, Timestamp matchDate,
					   int matchDay, String playCategory, String division, int totalPlayers, float totalPayment, int seasonId) {
		super();
		this.homeTeam = homeTeam;
		this.homeScore = homeScore;
		this.visitorScore = visitorScore;
		this.visitorTeam = visitorTeam;
		this.matchDate = matchDate;
		this.matchDay = matchDay;
		this.playCategory = playCategory;
		this.division = division;
		this.totalPlayers = totalPlayers;
		this.totalPayment = totalPayment;
		this.seasonId = seasonId;
	}
}
