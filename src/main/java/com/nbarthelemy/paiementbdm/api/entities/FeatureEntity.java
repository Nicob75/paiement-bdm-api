package com.nbarthelemy.paiementbdm.api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

import jakarta.persistence.*;
import java.util.Date;

import static jakarta.persistence.TemporalType.TIMESTAMP;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "features")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class FeatureEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="name")
	private String name	;
	@Column(name="htmldescription")
	private String htmlDescription;
	@Column(name="createddate", updatable = false)
	@CreatedDate
	@Temporal(TIMESTAMP)
	private Date createdDate;

	public FeatureEntity(String name, String htmlDescription, Date createdDate) {
		super();
		this.name = name;
		this.htmlDescription = htmlDescription;
		this.createdDate = createdDate;
	}
}
