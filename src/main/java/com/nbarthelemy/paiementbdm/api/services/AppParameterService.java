package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.common.PlayCategory;
import com.nbarthelemy.paiementbdm.api.config.AppParametersConfig;
import com.nbarthelemy.paiementbdm.api.repository.AppParameterRepository;
import org.springframework.stereotype.Service;

@Service
public class AppParameterService {
    private final AppParameterRepository appParameterRepository;
    private final AppParametersConfig    appParametersConfig;
    private final SeasonService          seasonService;

    public AppParameterService(
            AppParameterRepository appParameterRepository,
            AppParametersConfig appParametersConfig,
            SeasonService seasonService
                              ) {
        this.appParameterRepository = appParameterRepository;
        this.appParametersConfig = appParametersConfig;
        this.seasonService = seasonService;
    }

    public void setCurrentSeason( int seasonId ) {
        if ( seasonService.get( seasonId ) != null ) {
            var optionalCurrentSeasonAppParam = appParameterRepository.findById( AppParametersConfig.CURRENT_SEASON );
            if ( optionalCurrentSeasonAppParam.isPresent() ) {
                var currentSeasonAppParam = optionalCurrentSeasonAppParam.get();
                var currentSeasonIdString = "" + seasonId;
                currentSeasonAppParam.setAppValue( currentSeasonIdString );
                appParameterRepository.save( currentSeasonAppParam );
                appParametersConfig.getAppParameters().put( AppParametersConfig.CURRENT_SEASON, currentSeasonIdString );
            } else {
                throw new RuntimeException( "Could not find the " + optionalCurrentSeasonAppParam + " app param !" );
            }
        } else {
            throw new RuntimeException( "No season with id " + seasonId + " exist !" );
        }
    }

    public void setCurrentDivision( PlayCategory playCategory, String div) {
        String appParam;
        if ( playCategory.equals( PlayCategory.FOOT11 ) ) {
            appParam = AppParametersConfig.CURRENT_DIVISION_FOOT_11;
        } else {
            appParam = AppParametersConfig.CURRENT_DIVISION_FOOT_7;
        }
        var optionalCurrentSeasonAppParam = appParameterRepository.findById( appParam );
        if ( optionalCurrentSeasonAppParam.isPresent() ) {
            var currentSeasonAppParam = optionalCurrentSeasonAppParam.get();
            currentSeasonAppParam.setAppValue( div );
            appParameterRepository.save( currentSeasonAppParam );
        } else {
            throw new RuntimeException( "Could not find the " + optionalCurrentSeasonAppParam + " app param !" );
        }
    }
}
