package com.nbarthelemy.paiementbdm.api.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nbarthelemy.paiementbdm.api.common.PlayCategory;
import com.nbarthelemy.paiementbdm.api.common.StatusEnum;
import com.nbarthelemy.paiementbdm.api.config.AppParametersConfig;
import com.nbarthelemy.paiementbdm.api.config.EventConfig;
import com.nbarthelemy.paiementbdm.api.entities.EventEntity;
import com.nbarthelemy.paiementbdm.api.mappers.EventMapper;
import com.nbarthelemy.paiementbdm.api.model.Event;
import com.nbarthelemy.paiementbdm.api.model.EventPlayer;
import com.nbarthelemy.paiementbdm.api.model.SubmitEvent;
import com.nbarthelemy.paiementbdm.api.model.SubmitEventResult;
import com.nbarthelemy.paiementbdm.api.repository.EventRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class EventService {

    private static final String STR_BDM_FC = "BDM FC";

    private final EventRepository eventRepository;
    private final EventMapper eventMapper;

    private final EventPlayerService eventPlayerService;

    private final PlayerService playerService;

    private final EventConfig eventConfig;
    private final AppParametersConfig appParametersConfig;
    private final ObjectMapper objectMapper;

    public EventService( EventRepository eventRepository,
                         EventMapper eventMapper,
                         EventPlayerService eventPlayerService,
                         PlayerService playerService,
                         EventConfig eventConfig,
                         AppParametersConfig appParametersConfig ) {
        this.eventRepository = eventRepository;
        this.eventMapper = eventMapper;
        this.eventPlayerService = eventPlayerService;
        this.playerService = playerService;
        this.eventConfig = eventConfig;
        this.appParametersConfig = appParametersConfig;
        this.objectMapper = new ObjectMapper();
    }

    /**
     * Compute and set totalPlayersValid, totalPaymentValid, totalKeeperValid for the event in parameter
     * @param event the event to compute
     */
    public void processEvent(Event event) {
        // get the eventPlayers for this event
        List<EventPlayer> eventPlayers = this.eventPlayerService.getByEvent(event.getId());

        int computedTotalPlayersValid = eventPlayers.size();
        float computedTotalPaymentValid = (float)eventPlayers.stream()
                .mapToDouble(EventPlayer::getPayment)
                .sum();
        float computedTotalKeeperValid = (float)eventPlayers.stream()
                .mapToDouble(EventPlayer::getKeeper)
                .sum();

        event.setTotalPlayersValid(computedTotalPlayersValid == event.getTotalPlayers());
        event.setTotalPaymentValid(computedTotalPaymentValid == event.getTotalPayment());
        event.setTotalKeeperValid(computedTotalKeeperValid == event.getTotalKeeper());
    }

    /**
     * Get all the Event
     * @return All the Event
     */
    public List<Event> getAll() {
        List<EventEntity> eventEntities = this.eventRepository.findAll();
        List<Event> events = this.eventMapper.entitiesToModels(eventEntities);
        events.forEach(this::processEvent);
        return events;
    }

    /**
     * Get all the Event by Season id
     * @return All the Event by Season id
     */
    public List<Event> getBySeason(int id) {
        return this.getAll().stream()
                .filter(ev -> ev.getSeasonId() == id)
                .collect(Collectors.toList());
    }

    /**
     * Get the Event by Id if it exists
     * @param id the id
     * @return The Event if found, else null
     */
    public Event getById(int id) {
        EventEntity eventEntity = this.eventRepository.findById(id).orElse(null);
        return this.eventMapper.entityToModel(eventEntity);
    }

    /**
     * Get an Event (by Id)
     * @param event The Event
     * @return The Event if found, else null
     */
    public Event get(Event event) {
        return this.getById(event.getId());
    }

    /***
     * Create the Event passed in parameter if it does not exist
     * @param event The Event to be created
     * @return The created Event
     */
    public Event create(Event event) {
        verifyExistingEventById(event);
        verifyExistingSimilarEvent(event);

        EventEntity eventEntityToCreate = this.eventMapper.modelToEntity(event);
        EventEntity createdEventEntity = this.eventRepository.save(eventEntityToCreate);

        return this.eventMapper.entityToModel(createdEventEntity);
    }

    /***
     * Create the list of Event passed in parameter if they do not exist
     * @param events The list of Event to be created
     * @return The list of created Events
     */
    public List<Event> create(List<Event> events) {
        for (Event event : events) {
            verifyExistingEventById(event);
            verifyExistingSimilarEvent(event);
            verifyPlayCategory(event);
        }

        List<EventEntity> eventEntitiesToCreate = this.eventMapper.modelsToEntities(events);
        List<EventEntity> createdEventEntities = this.eventRepository.saveAll(eventEntitiesToCreate);

        return this.eventMapper.entitiesToModels(createdEventEntities);
    }

    /***
     * Convert the payload to a list of events and create them
     * @param sportEasyPayload The payload retrieved from SportEasy
     * @return The list of created Events
     */
    public Iterable<Event> createFromSportEasy(PlayCategory playCategory, int seasonId, String sportEasyPayload) throws JsonProcessingException {
        String eventsPayload = this.sportEasyPayloadToEventsPayload(playCategory, seasonId, sportEasyPayload);

        List<Event> events = this.parseEventsPayload(eventsPayload);

        // save
        return this.create(events);
    }

    // convert String payload to json payload (bulk events)
    public String sportEasyPayloadToEventsPayload(PlayCategory playCategory, int seasonId, String sportEasyPayload) {
        String sportEasyEventRegex = this.eventConfig.getSportEasyEventRegex();
        String division = getCurrentDivision( playCategory );
        String eventReplaceStr = this.eventConfig.getEventReplaceStr(playCategory, division, seasonId);

        if (sportEasyEventRegex == null) {
            throw new RuntimeException("Failed to load sport easy event regex !");
        }
        Pattern pattern = Pattern.compile(sportEasyEventRegex);
        Matcher matcher = pattern.matcher(sportEasyPayload);

        String eventsPayload = matcher.replaceAll(eventReplaceStr);
        eventsPayload = eventsPayload.strip();

        if (eventsPayload.length() > 0 && eventsPayload.charAt(eventsPayload.length() - 1) == ',') {
            eventsPayload = eventsPayload.substring(0, eventsPayload.length() - 1);
        }

        return "[" + eventsPayload + "]";
    }

    private String getCurrentDivision( PlayCategory playCategory ) {
        if ( playCategory.equals( PlayCategory.FOOT11 ) ) {
            return this.appParametersConfig.getAppParameters().get( AppParametersConfig.CURRENT_DIVISION_FOOT_11 );
        } else {
            return this.appParametersConfig.getAppParameters().get( AppParametersConfig.CURRENT_DIVISION_FOOT_7 );
        }
    }

    // convert json (bulk events) to list of events
    public List<Event> parseEventsPayload(String eventsPayload) throws JsonProcessingException {
        return this.objectMapper.readValue(eventsPayload, new TypeReference<>() {});
    }

    /**
     * Update the Event passed in parameter if it exists
     * @param event The Event to be updated
     * @return The updated Event
     */
    public Event update(Event event) {
        if (this.get(event) == null) {
            this.throwEventNotFound(event);
        }

        EventEntity eventEntityToUpdate = this.eventMapper.modelToEntity(event);
        EventEntity updatedEventEntity = this.eventRepository.save(eventEntityToUpdate);

        return this.eventMapper.entityToModel(updatedEventEntity);
    }

    /**
     * Update the Event passed in parameter if it exists
     * @param events The list of Event to be updated
     * @return The list of the created Event
     */
    public List<Event> update(List<Event> events) {
        for (Event event : events) {
            Event existingEventEntity = this.get(event);
            if (existingEventEntity == null) {
                this.throwEventNotFound(event);
            }
        }

        List<EventEntity> eventEntitiesToUpdate = this.eventMapper.modelsToEntities(events);
        List<EventEntity> updatedEventEntities = this.eventRepository.saveAll(eventEntitiesToUpdate);

        return this.eventMapper.entitiesToModels(updatedEventEntities);
    }

    /**
     * Submit the Event
     * ! We ignore the id that are passed !
     * We will just check that the EventId passed are correct
     * @param submitEvent The SubmitEvent to submit
     * @return The result of the submit (for submitEvent.eventPlayerList: only the created/updated lines)
     */
    @Transactional
    public SubmitEventResult submit(SubmitEvent submitEvent) {
        // prepare Event
        Event newEvent = submitEvent.getEvent();
        EventEntity existingEventEntity = this.eventRepository.findById(newEvent.getId()).orElse(null);
        if (existingEventEntity == null) {
            throw new RuntimeException("Event with id: '" + submitEvent.getEvent().getId()
                    + "' does not exist ! Can't update.");
        }

        newEvent.setId(existingEventEntity.getId());

        // prepare list of EventPlayer (matching on EventId)
        List<EventPlayer> allEventPlayerList = this.eventPlayerService.getAll(); // all EventPlayers
        List<EventPlayer> originalEventPlayerList = allEventPlayerList.stream()
                .filter(ep -> ep.getEventId() == newEvent.getId())
                .collect(Collectors.toList()); // only current Event
        List<EventPlayer> newEventPlayerList = submitEvent.getEventPlayerList();

        Map<EventPlayer, StatusEnum> eventPlayersToProcess = new HashMap<>();
        this.addEventPlayersToCreateUpdate(newEvent, originalEventPlayerList, newEventPlayerList, eventPlayersToProcess);
        this.addEventPlayersToDelete(originalEventPlayerList, newEventPlayerList, eventPlayersToProcess);

        this.processEventPlayers(eventPlayersToProcess);

        EventEntity eventEntityToCreate = this.eventMapper.modelToEntity(newEvent);
        EventEntity createdEventEntity = this.eventRepository.save(eventEntityToCreate);
        Event createdEvent = this.eventMapper.entityToModel(createdEventEntity);

        SubmitEventResult submitEventResult = new SubmitEventResult();
        submitEventResult.setEvent(createdEvent);
        submitEventResult.setProcessedEventPlayers(eventPlayersToProcess);
        submitEventResult.setResultEventPlayerList(newEventPlayerList);

        return submitEventResult;
    }

    /**
     * Generate the "eventPlayersToProcess" (create and update parts)
     * @param newEvent                The Event we want to submit
     * @param originalEventPlayerList The EventPlayer list before the submit
     * @param newEventPlayerList      The EventPlayer list we are trying to submit
     * @param eventPlayersToProcess   The EventPlayers we will process with their status
     */
    void addEventPlayersToCreateUpdate(
            Event newEvent,
            List<EventPlayer> originalEventPlayerList,
            List<EventPlayer> newEventPlayerList,
            Map<EventPlayer, StatusEnum> eventPlayersToProcess
    ) {
        for (EventPlayer newEventPlayer : newEventPlayerList) {
            if (newEventPlayer.getEventId() != newEvent.getId()) {
                throw new RuntimeException("One of the passed EventPlayer: '" + newEventPlayer
                        + "' does not have the correct eventId '" + newEventPlayer.getEventId()
                        + "'. Expected: '" + newEvent.getId() + "'. Can't submit.");
            }

            // find if an EventPlayer with that playerId already exists
            EventPlayer eventPlayer = originalEventPlayerList.stream()
                    .filter(ep -> ep.getPlayerId() == newEventPlayer.getPlayerId())
                    .findAny()
                    .orElse(null);

            // if the EventPlayer does not already exist we add it to our list to create
            if (eventPlayer == null) {
                // such an EventPlayer does not already exists, we can add it
                if (this.playerService.get(newEventPlayer.getPlayerId()) == null) {
                    throw new RuntimeException(
                            "newEventPlayer: No Player with id '" + newEventPlayer.getPlayerId()
                                    + "' exist ! Can't submit event."
                    );
                }

                EventPlayer eventPlayerToCreate = new EventPlayer(newEvent.getId(),
                        newEventPlayer.getPlayerId(),
                        newEventPlayer.getPayment(),
                        newEventPlayer.getKeeper());
                eventPlayersToProcess.put(eventPlayerToCreate, StatusEnum.CREATE);
            } else {
                // such an eventPlayer already exists and if the payment value or keeper value has changed,
                // we update the payment value
                if (newEventPlayer.getPayment() != eventPlayer.getPayment()
                        || newEventPlayer.getKeeper() != eventPlayer.getKeeper()) {
                    EventPlayer eventPlayerToUpdate = eventPlayer.toBuilder().build();
                    eventPlayerToUpdate.setPayment(newEventPlayer.getPayment());
                    eventPlayerToUpdate.setKeeper(newEventPlayer.getKeeper());
                    eventPlayersToProcess.put(eventPlayerToUpdate, StatusEnum.UPDATE);
                }
            }
        }
    }

    /**
     * Generate the "eventPlayersToProcess" (delete part)
     * @param originalEventPlayerEntityList The EventPlayer list before the submit
     * @param newEventPlayerEntityList      The EventPlayer list we are trying to submit
     * @param eventPlayersToProcess   The EventPlayers we will process with their status
     */
    void addEventPlayersToDelete(
            List<EventPlayer> originalEventPlayerEntityList,
            List<EventPlayer> newEventPlayerEntityList,
            Map<EventPlayer, StatusEnum> eventPlayersToProcess
    ) {
        for (EventPlayer eventPlayer : originalEventPlayerEntityList) {
            // find if an EventPlayer with that playerId exists in the input of the request
            EventPlayer existingEventPlayer = newEventPlayerEntityList.stream()
                    .filter(ep -> ep.getPlayerId() == eventPlayer.getPlayerId())
                    .findAny()
                    .orElse(null);

            // if no eventPlayersDTO found we add it to our list of EventPlayers to delete
            if (existingEventPlayer == null) {
                eventPlayersToProcess.put(eventPlayer, StatusEnum.DELETE);
            }
        }
    }

    /**
     * Create, update or delete the eventPlayers depending on the status formerly determined
     * @param eventPlayersToProcess the eventPlayers to be processed
     * @throws RuntimeException If we fail to process an eventPlayer
     */
    private void processEventPlayers(Map<EventPlayer, StatusEnum> eventPlayersToProcess) throws RuntimeException {
        for (Map.Entry<EventPlayer, StatusEnum> epToProcess : eventPlayersToProcess.entrySet()) {
            EventPlayer ep = epToProcess.getKey();
            switch (epToProcess.getValue()) {
                case CREATE:
                    this.eventPlayerService.create(ep);
                    break;
                case UPDATE:
                    this.eventPlayerService.update(ep);
                    break;
                case DELETE:
                    this.eventPlayerService.delete(ep);
                    break;
            }
        }
    }


    private void throwEventExistsById(Event event) {
        String msg = String.format(
                "Event with Id '%d' already exists ! Can't create. (homeTeam: %s, visitorTeam: %s)",
                event.getId(),
                event.getHomeTeam(),
                event.getVisitorTeam()
        );
        throw new ResponseStatusException(HttpStatus.CONFLICT, msg);
    }

    private void throwSimilarEventExists(Event event) {
        String msg = String.format(
                "Similar Event already exists ! Can't create. (homeTeam: %s, visitorTeam: %s, matchDay: %s, seasonId: %s)",
                event.getHomeTeam(),
                event.getVisitorTeam(),
                event.getMatchDay(),
                event.getSeasonId()
        );
        throw new ResponseStatusException(HttpStatus.CONFLICT, msg);
    }

    private void throwEventNotFound(Event event) {
        String msg = String.format(
                "Event with Id '%d' does not exists ! Can't update. (homeTeam: %s, visitorTeam: %s)",
                event.getId(),
                event.getHomeTeam(),
                event.getVisitorTeam()
        );
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, msg);
    }

    private void throwEventPlayCategoryWrongDayOfTheWeek(Event event, DayOfWeek dayOfTheWeek) {
        String msg = String.format(
                "A '%s' game cannot take place on a %s ! (homeTeam: %s, visitorTeam: %s, matchDay: %s, seasonId: %s)",
                event.getPlayCategory(),
                dayOfTheWeek.toString(),
                event.getHomeTeam(),
                event.getVisitorTeam(),
                event.getMatchDay(),
                event.getSeasonId()
        );
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg);
    }

    private Event findExistingEvent(Event event) {
        EventEntity eventEntity = (this.eventRepository.findAll()).stream()
                .filter(ev -> (ev.getHomeTeam().equals(event.getHomeTeam()) || event.getHomeTeam().contains(STR_BDM_FC))
                            && (ev.getVisitorTeam().equals(event.getVisitorTeam()) || event.getVisitorTeam().contains(STR_BDM_FC))
                            && ev.getMatchDay() == event.getMatchDay()
                            && ev.getSeasonId() == event.getSeasonId())
                .findFirst()
                .orElse(null);

        return this.eventMapper.entityToModel(eventEntity);
    }

    private void verifyExistingEventById(Event event) {
        Event existingEvent = this.get(event);

        if (existingEvent != null) {
            this.throwEventExistsById(event);
        }
    }

    private void verifyExistingSimilarEvent(Event event) {
        Event existingEvent = findExistingEvent(event);

        if (existingEvent != null) {
            this.throwSimilarEventExists(event);
        }
    }

    private void verifyPlayCategory(Event event) {
        LocalDate localDate = event.getMatchDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        DayOfWeek dayOfTheWeek = localDate.getDayOfWeek();

        if (event.getPlayCategory().equals(PlayCategory.FOOT7.getDisplayStr())) {
            if (dayOfTheWeek.equals(DayOfWeek.SATURDAY) || dayOfTheWeek.equals(DayOfWeek.SUNDAY)) {
                this.throwEventPlayCategoryWrongDayOfTheWeek(event, dayOfTheWeek);
            }
        } else if (event.getPlayCategory().equals(PlayCategory.FOOT11.getDisplayStr()) ){
            if (!dayOfTheWeek.equals(DayOfWeek.SATURDAY) && !dayOfTheWeek.equals(DayOfWeek.SUNDAY)) {
                this.throwEventPlayCategoryWrongDayOfTheWeek(event, dayOfTheWeek);
            }
        } else {
            String msg = String.format(
                "The play category '%s' does not exist ! (homeTeam: %s, visitorTeam: %s, matchDay: %s, seasonId: %s)",
                    event.getPlayCategory(),
                    event.getHomeTeam(),
                    event.getVisitorTeam(),
                    event.getMatchDay(),
                    event.getSeasonId()
            );
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg);
        }
    }


//	private Event getTestEvent() {
//		return new Event(
//				9,
//				"BDM FC A",
//				"5",
//				"2",
//				"Paris Lutece",
//				new Date(System.currentTimeMillis()),
//				7,
//				"Foot a 7",
//				"1"
//			);
//	}

}
