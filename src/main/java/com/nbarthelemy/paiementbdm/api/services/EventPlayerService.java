package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.entities.EventEntity;
import com.nbarthelemy.paiementbdm.api.entities.EventPlayerEntity;
import com.nbarthelemy.paiementbdm.api.mappers.EventMapper;
import com.nbarthelemy.paiementbdm.api.mappers.EventPlayerMapper;
import com.nbarthelemy.paiementbdm.api.model.Event;
import com.nbarthelemy.paiementbdm.api.model.EventPlayer;
import com.nbarthelemy.paiementbdm.api.repository.EventPlayerRepository;
import com.nbarthelemy.paiementbdm.api.repository.EventRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EventPlayerService {

    private final EventPlayerRepository eventPlayerRepository;
    private final EventPlayerMapper eventPlayerMapper;

    private final EventRepository eventRepository;
    private final EventMapper eventMapper;

    public EventPlayerService(
            EventPlayerRepository eventPlayerRepository,
            EventPlayerMapper eventPlayerMapper,
            EventRepository eventRepository,
            EventMapper eventMapper
    ) {
        this.eventPlayerRepository = eventPlayerRepository;
        this.eventPlayerMapper = eventPlayerMapper;
		this.eventRepository = eventRepository;
		this.eventMapper = eventMapper;
    }

    public List<EventPlayer> getAll() {
        List<EventPlayerEntity> eventEntities = this.eventPlayerRepository.findAll();
        return this.eventPlayerMapper.entitiesToModels(eventEntities);
    }

    /**
     * Get all the EventPlayer by Event (id)
     * @return All the EventPlayer by Event id
     */
    public List<EventPlayer> getByEvent(int id) {
        List<EventPlayerEntity> eventPlayerEntities = this.eventPlayerRepository.findAllByEventId(id);
        return this.eventPlayerMapper.entitiesToModels(eventPlayerEntities);
    }

    /**
     * Get all the EventPlayer by Season id
     * @return All the EventPlayer by Season id
     */
    public List<EventPlayer> getBySeason(int seasonId) {
        // get all the events of the season
		List<EventEntity> seasonEventEntities = this.eventRepository.findAllBySeasonId(seasonId);
		List<Event> seasonEvents = this.eventMapper.entitiesToModels(seasonEventEntities);
		List<Integer> seasonEventIds = seasonEvents.stream().map(Event::getId).collect(Collectors.toList());

        // get the corresponding event players
		List<EventPlayerEntity> eventPlayersEntities = this.eventPlayerRepository.findAll();
		List<EventPlayer> eventPlayers = this.eventPlayerMapper.entitiesToModels(eventPlayersEntities);

        return eventPlayers.stream()
				.filter(ep -> seasonEventIds.contains(ep.getEventId()) )
				.collect(Collectors.toList());
    }

    /**
     * Get the EventPlayer by Id if it exists
     * @param id the id
     * @return The EventPlayer if found, else null
     */
    public EventPlayer getById(int id) {
        EventPlayerEntity eventPlayerEntity = this.eventPlayerRepository.findById(id).orElse(null);
        return this.eventPlayerMapper.entityToModel(eventPlayerEntity);
    }

    /***
     * Create the EventPlayer passed in parameter if it does not exist
     * Additional existence criteria: same eventId and playerId
     *
     * @param eventPlayer The EventPlayer to be created
     * @return The created EventPlayer
     * @throws RuntimeException if the EventPlayer already exists
     */
    public EventPlayer create(EventPlayer eventPlayer) throws RuntimeException {
        Optional<EventPlayerEntity> checkedEventPlayer = this.eventPlayerRepository.findByEventIdAndPlayerId(eventPlayer.getEventId(), eventPlayer.getPlayerId());
        if (checkedEventPlayer.isPresent()) {
            EventPlayerEntity existingEventPlayerEntity = checkedEventPlayer.get();
            throw new RuntimeException(
                    String.format(
                            "An EventPlayer (Id: %d) with eventId '%d' and playerId %d already exists ! Can't create.",
                            existingEventPlayerEntity.getId(),
                            existingEventPlayerEntity.getEventId(),
                            existingEventPlayerEntity.getPlayerId()
                    )
            );
        }
        EventPlayerEntity eventPlayerToCreate = this.eventPlayerMapper.modelToEntity(eventPlayer);
        EventPlayerEntity createdEventPlayer = this.eventPlayerRepository.save(eventPlayerToCreate);
        return this.eventPlayerMapper.entityToModel(createdEventPlayer);
    }

    /**
     * Update the EventPlayer passed in parameter if it exists.
     * If no Id has been passed, we throw an exception.
     *
     * @param eventPlayer The EventPlayer to be updated
     * @return The updated EventPlayer
     * @throws RuntimeException if the EventPlayer does not exists
     */
    public EventPlayer update(EventPlayer eventPlayer) throws RuntimeException {
        Optional<EventPlayerEntity> checkedEventPlayer = this.eventPlayerRepository.findByEventIdAndPlayerId(eventPlayer.getEventId(), eventPlayer.getPlayerId());
        if (checkedEventPlayer.isEmpty()) {
            throw new RuntimeException(
                    String.format(
                            "No EventPlayer with eventId '%d' and playerId '%d' exist ! Can't updated. ",
                            eventPlayer.getEventId(),
                            eventPlayer.getPlayerId()
                    )
            );
        }
        eventPlayer.setId(checkedEventPlayer.get().getId());

        EventPlayerEntity eventPlayerToUpdate = this.eventPlayerMapper.modelToEntity(eventPlayer);
        EventPlayerEntity updatedEventPlayer = this.eventPlayerRepository.save(eventPlayerToUpdate);
        return this.eventPlayerMapper.entityToModel(updatedEventPlayer);
    }

    /**
     * Delete the EventPlayer by id
     *
     * @param id The id
     */
    public EventPlayer delete(int id) throws RuntimeException {
        Optional<EventPlayerEntity> optEventPlayerEntityToDelete = this.eventPlayerRepository.findById(id);
        if (optEventPlayerEntityToDelete.isPresent()) {
            EventPlayerEntity eventPlayerEntityToDelete = optEventPlayerEntityToDelete.get();
            this.eventPlayerRepository.deleteById(eventPlayerEntityToDelete.getId());
            return this.eventPlayerMapper.entityToModel(eventPlayerEntityToDelete);
        }
        throw new RuntimeException(String.format("EventPlayer with id: '%d' does not exist ! Can't delete.", id));
    }

    /**
     * Delete the EventPlayer (by id)
     *
     * @param eventPlayer The EventPlayer
     */
    public EventPlayer delete(EventPlayer eventPlayer) throws RuntimeException {
        return delete(eventPlayer.getId());
    }

//	private Event getTestEvent() {
//		return new Event(
//				9,
//				"BDM FC A",
//				"5",
//				"2",
//				"Paris Lutece",
//				new Date(System.currentTimeMillis()),
//				7,
//				"Foot a 7",
//				"1"
//			);
//	}

}