package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.entities.UserReadFeatureEntity;
import com.nbarthelemy.paiementbdm.api.model.UserReadFeature;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserReadFeatureMapper {
    UserReadFeatureEntity modelToEntity(UserReadFeature userReadFeature);
    UserReadFeature entityToModel(UserReadFeatureEntity userReadFeatureEntity);
}
