package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.model.EventPlayer;
import com.nbarthelemy.paiementbdm.api.entities.EventPlayerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EventPlayerMapper {
    EventPlayerEntity modelToEntity(EventPlayer eventplayer);
    EventPlayer entityToModel(EventPlayerEntity eventPlayerEntity);

    List<EventPlayerEntity> modelsToEntities(List<EventPlayer> eventplayers);
    List<EventPlayer> entitiesToModels(List<EventPlayerEntity> eventplayersEntities);
}
