package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.model.Event;
import com.nbarthelemy.paiementbdm.api.entities.EventEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EventMapper {
//    @Mapping(target = "id", ignore = true)
    EventEntity modelToEntity(Event event);
    Event entityToModel(EventEntity eventEntity);

    List<EventEntity> modelsToEntities(List<Event> event);
    List<Event> entitiesToModels(List<EventEntity> eventEntities);
}
