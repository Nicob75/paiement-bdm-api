package com.nbarthelemy.paiementbdm.api.util;

public class StringUtil {
    public static String normalizeEOL(String inputStr) {
        return inputStr.replace("\r\n", "\n");
    }
}
