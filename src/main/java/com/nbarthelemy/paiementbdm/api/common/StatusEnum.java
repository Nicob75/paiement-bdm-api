package com.nbarthelemy.paiementbdm.api.common;

public enum StatusEnum {
    CREATE,
    UPDATE,
    DELETE
}
