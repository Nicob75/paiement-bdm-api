package com.nbarthelemy.paiementbdm.api;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringDocConfig {
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info( new Info()
                               .title( "Paiement BDM API" )
                               .version( "1.0.0" )
                               .description( "The paiement BDM API" ) )
                .addSecurityItem( new SecurityRequirement().addList( "APIKEY" ) )
                .components( new io.swagger.v3.oas.models.Components()
                                     .addSecuritySchemes( "APIKEY", new SecurityScheme()
                                                                  .name( "Authorization" )
                                                                  .type( SecurityScheme.Type.APIKEY )
                                                                  .in( SecurityScheme.In.HEADER )
                                                        ) );
    }
}
