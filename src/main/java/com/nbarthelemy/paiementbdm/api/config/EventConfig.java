package com.nbarthelemy.paiementbdm.api.config;

import com.nbarthelemy.paiementbdm.api.common.PlayCategory;
import com.nbarthelemy.paiementbdm.api.util.StringUtil;
import jakarta.annotation.PostConstruct;
import lombok.Data;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;

@Data
@Configuration
public class EventConfig {
    private String sportEasyEventRegex;
    private String             desktopEventReplace;
    public static final String SEASON_ID_STR = "{{{SEASON_ID}}}";
    public static final String PLAY_CATEGORY_STR = "{{{PLAY_CATEGORY}}}";
    public static final String DIVISION_STR      = "{{{DIVISION_STR}}}";

    // can be used to load or reload the configuration
    @PostConstruct
    public void reload() throws IOException {
        this.loadSportEasyEventRegex();
        this.loadDesktopEventReplace();
    }

    public String getEventReplaceStr(PlayCategory playCategory, String division, int seasonId) {
        var result = this.desktopEventReplace.replace( PLAY_CATEGORY_STR, playCategory.getDisplayStr() );
        result = result.replace( DIVISION_STR, division );
        result = result.replace(SEASON_ID_STR, Integer.toString(seasonId));
        return result;
    }

    // load sport easy event regex
    private void loadSportEasyEventRegex() throws IOException {
        try ( InputStream inputStream = new ClassPathResource("events/sport-easy-event-regex.txt").getInputStream() ) {
            String newSportEasyEventRegex = new String(inputStream.readAllBytes());
            this.sportEasyEventRegex = StringUtil.normalizeEOL(newSportEasyEventRegex);
        }
    }

    // load foot11 event replace
    private void loadDesktopEventReplace() throws IOException {
        try ( InputStream inputStream = new ClassPathResource("events/desktop-event-replace.txt").getInputStream() ) {
            String newFoot11EventReplace = new String(inputStream.readAllBytes());
            this.desktopEventReplace = StringUtil.normalizeEOL( newFoot11EventReplace );
        }
    }
}
