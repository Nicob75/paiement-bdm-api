package com.nbarthelemy.paiementbdm.api.controllers.handler;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Object> handleUncaughtDataIntegrityViolationException(Exception ex) {
        CustomErrorResponse errors = new CustomErrorResponse();
        errors.setTimestamp(LocalDateTime.now());
        errors.setError(ex.getMessage());
        String stackTrace = printExceptionStackTrace(ex);
        errors.setTrace(stackTrace);
        logger.error(stackTrace);
        HttpStatus status = HttpStatus.CONFLICT;
        errors.setStatus(status.value());
        errors.setStatusPhrase(status.getReasonPhrase());

        return new ResponseEntity<>(errors, status);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> handleUncaughtException(Exception ex) {
        CustomErrorResponse errors = new CustomErrorResponse();
        errors.setTimestamp(LocalDateTime.now());
        errors.setError(ex.getMessage());
        String stackTrace = printExceptionStackTrace(ex);
        errors.setTrace(stackTrace);
        logger.error(stackTrace);
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        errors.setStatus(status.value());
        errors.setStatusPhrase(status.getReasonPhrase());

        return new ResponseEntity<>(errors, status);
    }

    private String printExceptionStackTrace(Throwable exception) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);

        return sw.toString();
    }
}
