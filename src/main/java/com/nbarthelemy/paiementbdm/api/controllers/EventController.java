package com.nbarthelemy.paiementbdm.api.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nbarthelemy.paiementbdm.api.common.PlayCategory;
import com.nbarthelemy.paiementbdm.api.model.Event;
import com.nbarthelemy.paiementbdm.api.model.SubmitEvent;
import com.nbarthelemy.paiementbdm.api.model.SubmitEventResult;
import com.nbarthelemy.paiementbdm.api.services.EventService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
@CrossOrigin
@RequestMapping("/event")
public class EventController {
	
	private final EventService eventService;
	
	public EventController(EventService eventService) {
		this.eventService = eventService;
	}

    @ResponseBody
    @GetMapping(value = "/all")
    public List<Event> getAll() {
	    return this.eventService.getAll();
    }

    @ResponseBody
    @GetMapping(value = "/season/{id}")
    public List<Event> getBySeason(@PathVariable("id") int id) {
        return this.eventService.getBySeason(id);
    }

    @ResponseBody
    @GetMapping(value = "/{id}")
    public Event get(@PathVariable("id") int id) {
	    Event event = this.eventService.getById(id);
        if (event != null) {
            return event;
        }
	    throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Event not found");
    }

    @ResponseBody
    @PostMapping(value = "")
    public Event create(@RequestBody Event event) {
        return this.eventService.create(event);
    }

    @Secured({"ROLE_MANAGER"})
    @ResponseBody
    @PostMapping(value = "/bulk")
    public List<Event> create(@RequestBody List<Event> events) {
        return this.eventService.create(events);
    }

    @Secured({"ROLE_MANAGER"})
    @ResponseBody
    @PostMapping(value = "/create/from-sport-easy")
    public Iterable<Event> createFromSportEasy(@RequestParam PlayCategory playCategory,
                                               @RequestParam int seasonId,
                                               @RequestBody String sportEasyPayload) throws JsonProcessingException {
        return this.eventService.createFromSportEasy(playCategory, seasonId, sportEasyPayload);
    }

    @Secured({"ROLE_MANAGER"})
    @ResponseBody
    @PutMapping(value = "/{id}")
    public Event update(@PathVariable("id") int id, @RequestBody Event event) {
        event.setId(id);
        return this.eventService.update(event);
    }

    @Secured({"ROLE_MANAGER"})
    @ResponseBody
    @PutMapping(value = "/bulk")
    public List<Event> update(@RequestBody List<Event> events) {
        return this.eventService.update(events);
    }

    @Secured({"ROLE_MANAGER"})
    @ResponseBody
    @PostMapping(value = "/submit")
    public SubmitEventResult submit(@RequestBody @Validated SubmitEvent submitEvent) {
        return this.eventService.submit(submitEvent);
    }
	
	/*private Event getTestEvent() {
		return new Event(
				5,
				"BDM FC A",
				"5",
				"2",
				"Paris Lutece",
				new Date(System.currentTimeMillis()),
				7,
				"Foot a 7",
				"1"
			);
	}*/
}
