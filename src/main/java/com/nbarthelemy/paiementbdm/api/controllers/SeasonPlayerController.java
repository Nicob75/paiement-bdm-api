package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.entities.SeasonPlayerEntity;
import com.nbarthelemy.paiementbdm.api.mappers.SeasonPlayerMapper;
import com.nbarthelemy.paiementbdm.api.model.SeasonPlayer;
import com.nbarthelemy.paiementbdm.api.repository.SeasonPlayerRepository;
import com.nbarthelemy.paiementbdm.api.services.SeasonPlayerService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
@CrossOrigin
@RequestMapping("/seasonplayer")
public class SeasonPlayerController {

	private final SeasonPlayerService seasonPlayerService;
	private final SeasonPlayerMapper seasonPlayerMapper;
	private final SeasonPlayerRepository seasonPlayerRepository;

	public SeasonPlayerController(SeasonPlayerService seasonPlayerService,
								  SeasonPlayerMapper seasonPlayerMapper,
								  SeasonPlayerRepository seasonPlayerRepository) {
		this.seasonPlayerService = seasonPlayerService;
		this.seasonPlayerMapper = seasonPlayerMapper;
		this.seasonPlayerRepository = seasonPlayerRepository;
	}

    @ResponseBody
    @GetMapping(value = "/all")
    public List<SeasonPlayer> getAll() {
		List<SeasonPlayerEntity> seasonPlayerEntities = this.seasonPlayerRepository.findAll();
		return this.seasonPlayerMapper.entitiesToModels(seasonPlayerEntities);
	}

	@ResponseBody
	@GetMapping(value = "/{id}")
	public SeasonPlayer get(@PathVariable("id") int id) {
		SeasonPlayer seasonPlayer = this.seasonPlayerService.get(id);
		if (seasonPlayer != null) {
			return seasonPlayer;
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "EventPlayer not found");
	}

	@ResponseBody
	@GetMapping(value = "/season/{id}")
	public List<SeasonPlayer> getBySeason(@PathVariable("id") int id) {
		return this.seasonPlayerService.getBySeason(id);
	}

	@ResponseBody
	@GetMapping(value = "/player/{id}")
	public List<SeasonPlayer> getByPlayer(@PathVariable("id") int id) {
		return this.seasonPlayerService.getByPlayer(id);
	}

	@Secured({"ROLE_MANAGER"})
	@ResponseBody
	@PostMapping(value = "")
	public SeasonPlayer create(@RequestBody SeasonPlayer seasonPlayer) throws Exception {
		return this.seasonPlayerService.create(seasonPlayer);
	}

	@Secured({"ROLE_MANAGER"})
    @ResponseBody
    @PutMapping(value = "/{id}")
    public SeasonPlayer update(@PathVariable("id") int id, @RequestBody SeasonPlayer seasonPlayer) throws Exception {
		seasonPlayer.setId(id);
        return this.seasonPlayerService.update(seasonPlayer);
    }

	@Secured({"ROLE_MANAGER"})
    @ResponseBody
    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") int id) {
        this.seasonPlayerService.delete(id);
    }

	@Secured({"ROLE_MANAGER"})
	@ResponseBody
	@DeleteMapping(value = "season/{seasonId}/player/{playerId}")
	public void deleteByPlayerId(@PathVariable("seasonId") int seasonId, @PathVariable("playerId") int playerId) {
		this.seasonPlayerService.deleteBySeasonAndPlayerId(seasonId, playerId);
	}
	
	/*private Event getTestEvent() {
		return new Event(
				5,
				"BDM FC A",
				"5",
				"2",
				"Paris Lutece",
				new Date(System.currentTimeMillis()),
				7,
				"Foot a 7",
				"1"
			);
	}*/
}
