package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.common.PlayCategory;
import com.nbarthelemy.paiementbdm.api.services.AppParameterService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@RequestMapping("/param")
public class AppParameterController {

    private final AppParameterService appParameterService;

	public AppParameterController( AppParameterService appParameterService ) {
		this.appParameterService = appParameterService;
	}

    @ResponseBody
    @Secured( {"ROLE_ADMIN"} )
    @PutMapping(value = "/current-season/{id}")
    public void updateCurrentSeason(@PathVariable("id") int seasonId ) {
        this.appParameterService.setCurrentSeason(seasonId);
    }

    @ResponseBody
    @Secured( {"ROLE_ADMIN"} )
    @PutMapping(value = "/current-division-foot-11/{div}")
    public void updateCurrentDivisionFoot11(@PathVariable("div") String currentDivisionFoot11) {
        this.appParameterService.setCurrentDivision( PlayCategory.FOOT11, currentDivisionFoot11 );
    }

    @ResponseBody
    @Secured( {"ROLE_ADMIN"} )
    @PutMapping(value = "/current-division-foot-7/{div}")
    public void updateCurrentDivisionFoot77(@PathVariable("div") String currentDivisionFoot7) {
        this.appParameterService.setCurrentDivision( PlayCategory.FOOT7, currentDivisionFoot7);
    }
}
