package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.entities.ERole;
import com.nbarthelemy.paiementbdm.api.entities.RoleEntity;
import com.nbarthelemy.paiementbdm.api.entities.UserEntity;
import com.nbarthelemy.paiementbdm.api.repository.RoleRepository;
import com.nbarthelemy.paiementbdm.api.repository.UserRepository;
import com.nbarthelemy.paiementbdm.api.security.jwt.JwtUtils;
import com.nbarthelemy.paiementbdm.api.security.payload.request.ChangePasswordRequest;
import com.nbarthelemy.paiementbdm.api.security.payload.request.ExpirePasswordRequest;
import com.nbarthelemy.paiementbdm.api.security.payload.request.LoginRequest;
import com.nbarthelemy.paiementbdm.api.security.payload.request.SignupRequest;
import com.nbarthelemy.paiementbdm.api.security.payload.response.JwtResponse;
import com.nbarthelemy.paiementbdm.api.security.payload.response.MessageResponse;
import com.nbarthelemy.paiementbdm.api.security.service.UserDetailsImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {
    private AuthenticationManager authenticationManager;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder encoder;
    private JwtUtils jwtUtils;

    public AuthController(
            AuthenticationManager authenticationManager,
            UserRepository userRepository,
            RoleRepository roleRepository,
            PasswordEncoder encoder,
            JwtUtils jwtUtils
    ) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
    }

    private void updateUserLastLogin(UserDetailsImpl userDetails) {
        long id = userDetails.getId();
        Optional<UserEntity> optUser = userRepository.findById(id);
        if (optUser.isEmpty()) {
            throw new RuntimeException("Error: impossible de trouver l'utilisateur avec l'id: " + id);
        }

        UserEntity userEntity = optUser.get();
        userEntity.setLastLoginDate(new Date());
        userRepository.save(userEntity);
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            List<String> roles = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toList());

            updateUserLastLogin(userDetails);

            return ResponseEntity.ok(
                    new JwtResponse(
                            jwt,
                            userDetails.getId(),
                            userDetails.getUsername(),
                            userDetails.getEmail(),
                            roles
                    )
            );
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        try {
            if (userRepository.existsByUsername(signUpRequest.getUsername())) {
                throw new RuntimeException("Error: Le nom d\'utilisateur est deja pris !");
            }

            if (userRepository.existsByEmail(signUpRequest.getEmail())) {
                throw new RuntimeException("Error: Email deja utilise !");
            }

            // Create new user's account
            UserEntity userEntity = new UserEntity(signUpRequest.getUsername(),
                    signUpRequest.getEmail(),
                    encoder.encode(signUpRequest.getPassword()));

            Set<String> strRoles = signUpRequest.getRole();
            Set<RoleEntity> roleEntities = new HashSet<>();

            if (strRoles == null) {
                RoleEntity userRoleEntity = roleRepository.findByName(ERole.ROLE_USER)
                        .orElseThrow(() -> new RuntimeException("Error: Role non trouve."));
                roleEntities.add(userRoleEntity);
            } else {
                strRoles.forEach(role -> {
                    switch (role) {
                        case "admin":
                            RoleEntity adminRoleEntity = roleRepository.findByName(ERole.ROLE_ADMIN)
                                    .orElseThrow(() -> new RuntimeException("Error: Role non trouve."));
                            roleEntities.add(adminRoleEntity);

                            break;
                        case "manager":
                            RoleEntity modRoleEntity = roleRepository.findByName(ERole.ROLE_MANAGER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role non trouve."));
                            roleEntities.add(modRoleEntity);

                            break;
                        default:
                            RoleEntity userRoleEntity = roleRepository.findByName(ERole.ROLE_USER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role non trouve."));
                            roleEntities.add(userRoleEntity);
                    }
                });
            }

            userEntity.setRoleEntities(roleEntities);
            userRepository.save(userEntity);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new MessageResponse("Utilisateur enregistre avec succes !"));
    }

    @PostMapping("/password/change")
    public ResponseEntity<?> changePassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
        try {
            Optional<UserEntity> optUser = userRepository.findByUsername(changePasswordRequest.getUsername());
            if (optUser.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            UserEntity userEntity = optUser.get();
            if (!userEntity.isPasswordExpired()) {
                throw new RuntimeException("Le mot de passe de l'utilisateur n'est pas expire !");
            }

            userEntity.setPassword(encoder.encode(changePasswordRequest.getPassword()));
            userEntity.setPasswordExpired(false);
            this.userRepository.save(userEntity);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new MessageResponse("Mot de passe change avec succes !"));
    }

    @Secured({"ROLE_ADMIN"})
    @PostMapping("/password/expire")
    public ResponseEntity<?> expirePassword(@Valid @RequestBody ExpirePasswordRequest expirePasswordRequest) {
        try {
            Optional<UserEntity> optUser = userRepository.findByUsername(expirePasswordRequest.getUsername());
            if (optUser.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            UserEntity userEntity = optUser.get();
            userEntity.setPasswordExpired(true);
            this.userRepository.save(userEntity);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(new MessageResponse("Mot de passe expire avec succes !"));
    }
}
