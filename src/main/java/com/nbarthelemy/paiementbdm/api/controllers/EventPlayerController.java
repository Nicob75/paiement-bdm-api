package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.entities.EventPlayerEntity;
import com.nbarthelemy.paiementbdm.api.mappers.EventPlayerMapper;
import com.nbarthelemy.paiementbdm.api.model.EventPlayer;
import com.nbarthelemy.paiementbdm.api.repository.EventPlayerRepository;
import com.nbarthelemy.paiementbdm.api.services.EventPlayerService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
@CrossOrigin
@RequestMapping("/eventplayer")
public class EventPlayerController {
	
	private final EventPlayerService eventPlayerService;
	private final EventPlayerMapper eventPlayerMapper;
	private final EventPlayerRepository eventPlayerRepository;
	
	public EventPlayerController(EventPlayerService eventPlayerService,
								 EventPlayerMapper eventPlayerMapper,
								 EventPlayerRepository eventPlayerRepository) {
		this.eventPlayerService = eventPlayerService;
		this.eventPlayerMapper = eventPlayerMapper;
		this.eventPlayerRepository = eventPlayerRepository;
	}

    @ResponseBody
    @GetMapping(value = "/all")
    public List<EventPlayer> getAll() {
		return this.eventPlayerService.getAll();
	}

	@ResponseBody
	@GetMapping(value = "/season/{id}")
	public List<EventPlayer> getBySeason(@PathVariable("id") int seasonId) {
		return this.eventPlayerService.getBySeason(seasonId);
	}

	@ResponseBody
	@GetMapping(value = "/{id}")
	public EventPlayer get(@PathVariable("id") int id) {
		EventPlayer eventPlayer = this.eventPlayerService.getById(id);
		if (eventPlayer != null) {
			return eventPlayer;
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "EventPlayer not found");
	}

	@ResponseBody
	@GetMapping(value = "/event/{id}")
	public List<EventPlayer> getByEvent(@PathVariable("id") int id) {
		return this.eventPlayerService.getByEvent(id);
	}

	@ResponseBody
	@GetMapping(value = "/player/{id}")
	public List<EventPlayer> getByPlayer(@PathVariable("id") int id) {
		List<EventPlayerEntity> eventPlayerEntities = this.eventPlayerRepository.findAllByPlayerId(id);
		return this.eventPlayerMapper.entitiesToModels(eventPlayerEntities);
	}

	@Secured({"ROLE_MANAGER"})
	@ResponseBody
	@PostMapping(value = "")
	public EventPlayer create(@RequestBody EventPlayer eventPlayer) throws RuntimeException {
		return this.eventPlayerService.create(eventPlayer);
	}

	@Secured({"ROLE_MANAGER"})
    @ResponseBody
    @PutMapping(value = "/{id}")
    public EventPlayer update(@PathVariable("id") int id, @RequestBody EventPlayer eventPlayer) throws RuntimeException {
		eventPlayer.setId(id);
        return this.eventPlayerService.update(eventPlayer);
    }

	@Secured({"ROLE_MANAGER"})
    @ResponseBody
    @DeleteMapping(value = "/{id}")
    public EventPlayer delete(@PathVariable("id") int id) throws RuntimeException {
        return this.eventPlayerService.delete(id);
    }
	
	/*private Event getTestEvent() {
		return new Event(
				5,
				"BDM FC A",
				"5",
				"2",
				"Paris Lutece",
				new Date(System.currentTimeMillis()),
				7,
				"Foot a 7",
				"1"
			);
	}*/
}
