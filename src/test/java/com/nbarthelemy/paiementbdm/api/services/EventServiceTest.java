package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.common.StatusEnum;
import com.nbarthelemy.paiementbdm.api.entities.EventEntity;
import com.nbarthelemy.paiementbdm.api.entities.EventPlayerEntity;
import com.nbarthelemy.paiementbdm.api.mappers.EventMapper;
import com.nbarthelemy.paiementbdm.api.mappers.EventMapperImpl;
import com.nbarthelemy.paiementbdm.api.mappers.EventMapperTests;
import com.nbarthelemy.paiementbdm.api.mappers.EventPlayerMapper;
import com.nbarthelemy.paiementbdm.api.mappers.EventPlayerMapperImpl;
import com.nbarthelemy.paiementbdm.api.mappers.EventPlayerMapperTests;
import com.nbarthelemy.paiementbdm.api.model.Event;
import com.nbarthelemy.paiementbdm.api.model.EventPlayer;
import com.nbarthelemy.paiementbdm.api.model.Player;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith( MockitoExtension.class )
public class EventServiceTest {

    @InjectMocks
    private EventService eventService;

    @Mock
    private PlayerService playerService;

    public static Event getEventMock(Event event, int id) {
        EventMapper eventMapper = new EventMapperImpl();
        EventEntity eventEntity = eventMapper.modelToEntity(event);
        eventEntity.setId(id);

        return eventMapper.entityToModel(eventEntity);
    }

    public static EventPlayer getEventPlayerMock(EventPlayer eventPlayer, int id) {
        EventPlayerMapper eventPlayerMapper = new EventPlayerMapperImpl();
        EventPlayerEntity eventPlayerEntity = eventPlayerMapper.modelToEntity(eventPlayer);
        eventPlayerEntity.setId(id);

        return eventPlayerMapper.entityToModel(eventPlayerEntity);
    }

    public static Player getPlayerMock(int id) {
        Player player = new Player();
        player.setId(id);

        return player;
    }

    @Test
    void addEventPlayersToCreateTest() {
        Event event = getEventMock(EventMapperTests.getEventDTOMock(), 1);
        EventPlayer eventPlayer1 = getEventPlayerMock(EventPlayerMapperTests.getEventPlayerDTOMock1(), 1);
        EventPlayer eventPlayer2 = getEventPlayerMock(EventPlayerMapperTests.getEventPlayerDTOMock2(), 2);
        EventPlayer eventPlayer3 = getEventPlayerMock (EventPlayerMapperTests.getEventPlayerDTOMock3(), 3);
        Player dummyPlayer = getPlayerMock(7);

        List<EventPlayer> originalEventPlayers = Stream.of(
                eventPlayer1, eventPlayer2
        ).toList();
        List<EventPlayer> targetEventPlayers = Stream.of(
                eventPlayer1, eventPlayer2, eventPlayer3
        ).toList();

        when(this.playerService.get(anyInt())).thenReturn(dummyPlayer);

        Map<EventPlayer, StatusEnum> eventPlayersToProcess = new HashMap<>();

        this.eventService.addEventPlayersToCreateUpdate(
                event,
                originalEventPlayers,
                targetEventPlayers,
                eventPlayersToProcess
        );

        double targetSum = targetEventPlayers.stream()
                .mapToDouble(EventPlayer::getPayment)
                .sum();
        double expectedSum = 25.0f;

        assertThat( targetSum ).isEqualTo( expectedSum );
        assertThat( eventPlayersToProcess ).hasSize( 1 );

        Map.Entry<EventPlayer, StatusEnum> newEp = eventPlayersToProcess.entrySet().iterator().next();

        assertThat( newEp.getValue() ).isEqualTo( StatusEnum.CREATE );
    }

    @Test
    void addEventPlayersToUpdateTest() {
        Event event = getEventMock(EventMapperTests.getEventDTOMock(), 1);
        EventPlayer eventPlayer1 = getEventPlayerMock(EventPlayerMapperTests.getEventPlayerDTOMock1(), 1);
        EventPlayer eventPlayer2 = getEventPlayerMock(EventPlayerMapperTests.getEventPlayerDTOMock2(), 2);
        EventPlayer eventPlayer2_target = getEventPlayerMock(EventPlayerMapperTests.getEventPlayerDTOMock2(), 2);
        eventPlayer2_target.setPayment(3.0f);

        List<EventPlayer> originalEventPlayers = Stream.of(
                eventPlayer1, eventPlayer2
        ).toList();
        List<EventPlayer> targetEventPlayers = Stream.of(
                eventPlayer1, eventPlayer2_target
        ).toList();

        Map<EventPlayer, StatusEnum> eventPlayersToProcess = new HashMap<>();

        this.eventService.addEventPlayersToCreateUpdate(
                event,
                originalEventPlayers,
                targetEventPlayers,
                eventPlayersToProcess
        );

        double targetSum = targetEventPlayers.stream()
                .mapToDouble(EventPlayer::getPayment)
                .sum();
        double expectedSum = 23.0f;

        assertThat( targetSum ).isEqualTo( expectedSum );
        assertThat( eventPlayersToProcess ).hasSize( 1 );

        Map.Entry<EventPlayer, StatusEnum> newEp = eventPlayersToProcess.entrySet().iterator().next();

        assertThat( newEp.getValue() ).isEqualTo( StatusEnum.UPDATE );
    }

    @Test
    void addEventPlayersToDeleteTest() {
        EventPlayer eventPlayer1 = getEventPlayerMock(EventPlayerMapperTests.getEventPlayerDTOMock1(), 1);
        EventPlayer eventPlayer2 = getEventPlayerMock(EventPlayerMapperTests.getEventPlayerDTOMock2(), 2);

        List<EventPlayer> originalEventPlayers = Stream.of(
                eventPlayer1, eventPlayer2
        ).toList();
        List<EventPlayer> targetEventPlayers = Stream.of(
                eventPlayer2
        ).toList();

        Map<EventPlayer, StatusEnum> eventPlayersToProcess = new HashMap<>();

        this.eventService.addEventPlayersToDelete(
                originalEventPlayers,
                targetEventPlayers,
                eventPlayersToProcess
        );

        double targetSum = targetEventPlayers.stream()
                .mapToDouble(EventPlayer::getPayment)
                .sum();
        double expectedSum = 0.0f;

        assertThat( targetSum ).isEqualTo( expectedSum );
        assertThat( eventPlayersToProcess ).hasSize( 1 );

        Map.Entry<EventPlayer, StatusEnum> newEp = eventPlayersToProcess.entrySet().iterator().next();

        assertThat( newEp.getValue() ).isEqualTo( StatusEnum.DELETE );
    }

}
