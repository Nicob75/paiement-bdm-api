package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.entities.EventPlayerEntity;
import com.nbarthelemy.paiementbdm.api.model.EventPlayer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith( SpringExtension.class )
@SpringBootTest()
public class EventPlayerMapperTests {

	@Autowired
	private EventPlayerMapper eventPlayerMapper;

	public static EventPlayer getEventPlayerDTOMock1() {
		EventPlayer eventPlayer = new EventPlayer();
		eventPlayer.setEventId(1);
		eventPlayer.setPlayerId(1);
		eventPlayer.setPayment(20.0f);

		return eventPlayer;
	}

	public static EventPlayer getEventPlayerDTOMock2() {
		EventPlayer eventPlayer = new EventPlayer();
		eventPlayer.setEventId(1);
		eventPlayer.setPlayerId(5);
		eventPlayer.setPayment(0.0f);

		return eventPlayer;
	}

	public static EventPlayer getEventPlayerDTOMock3() {
		EventPlayer eventPlayer = new EventPlayer();
		eventPlayer.setEventId(1);
		eventPlayer.setPlayerId(7);
		eventPlayer.setPayment(5.0f);

		return eventPlayer;
	}

	@Test
	void eventPlayerMapperTest() {
		EventPlayer eventPlayer = EventPlayerMapperTests.getEventPlayerDTOMock1();
		EventPlayerEntity eventPlayerEntity = eventPlayerMapper.modelToEntity(eventPlayer);

		assertThat( eventPlayer.getEventId() ).isEqualTo( eventPlayerEntity.getEventId() );
		assertThat( eventPlayer.getPlayerId() ).isEqualTo( eventPlayerEntity.getPlayerId() );
		assertThat( eventPlayer.getPayment() ).isEqualTo( eventPlayerEntity.getPayment() );
	}

}
