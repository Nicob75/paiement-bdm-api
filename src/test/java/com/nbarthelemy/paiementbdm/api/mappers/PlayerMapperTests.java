package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.entities.PlayerEntity;
import com.nbarthelemy.paiementbdm.api.model.Player;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith( SpringExtension.class )
@SpringBootTest()
class PlayerMapperTests {

	@Autowired
	private PlayerMapper playerMapper;

	@Test
	void playerMapperTest() {
		Player player = new Player();
		player.setBdmId(1);
		player.setFirstname("dtoFirstName");
		player.setLastname("dtoLastName");
		player.setJerseyNumber(1);
		PlayerEntity playerEntity = playerMapper.modelToEntity(player);

		assertThat( player.getBdmId() ).isEqualTo( playerEntity.getBdmId() );
		assertThat( player.getFirstname() ).isEqualTo( playerEntity.getFirstname() );
		assertThat( player.getLastname() ).isEqualTo( playerEntity.getLastname() );
		assertThat( player.getJerseyNumber() ).isEqualTo( playerEntity.getJerseyNumber() );
	}

}
