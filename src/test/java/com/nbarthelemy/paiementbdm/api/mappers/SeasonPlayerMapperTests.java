package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.entities.SeasonPlayerEntity;
import com.nbarthelemy.paiementbdm.api.model.SeasonPlayer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith( SpringExtension.class )
@SpringBootTest()
class SeasonPlayerMapperTests {

	@Autowired
	private SeasonPlayerMapper seasonPlayerMapper;

	@Test
	void seasonPlayerMapperTest() {
		SeasonPlayer seasonPlayer = new SeasonPlayer();
		seasonPlayer.setSeasonId(1);
		seasonPlayer.setPlayerId(1);
		SeasonPlayerEntity seasonPlayerEntity = seasonPlayerMapper.modelToEntity(seasonPlayer);

		assertThat(seasonPlayer.getSeasonId()).isEqualTo( seasonPlayerEntity.getSeasonId() );
		assertThat(seasonPlayer.getPlayerId()).isEqualTo( seasonPlayerEntity.getPlayerId() );
	}

}
