package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.entities.EventEntity;
import com.nbarthelemy.paiementbdm.api.model.Event;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith( SpringExtension.class )
@SpringBootTest()
public class EventMapperTests {

	@Autowired
	private EventMapper eventMapper;

	public static Event getEventDTOMock() {
		Event event = new Event();
		event.setHomeTeam("dtoHomeTeam");
		event.setHomeScore("dtoHomeScore");
		event.setVisitorScore("dtoVisitorScore");
		event.setVisitorTeam("dtoVisitorTeam");
		event.setMatchDate(new Date());
		event.setMatchDay(99);
		event.setPlayCategory("dtoPlayCategory");
		event.setDivision("dtoDivision");
		event.setTotalPlayers(10);
		event.setTotalPayment(30.0f);
		event.setSeasonId(9);

		return event;
	}

	@Test
	void eventMapperTest() {
		Event event = EventMapperTests.getEventDTOMock();
		EventEntity eventEntity = this.eventMapper.modelToEntity(event);

		assertThat( event.getHomeTeam() ).isEqualTo( eventEntity.getHomeTeam() );
		assertThat( event.getHomeScore() ).isEqualTo( eventEntity.getHomeScore() );
		assertThat( event.getVisitorScore() ).isEqualTo( eventEntity.getVisitorScore() );
		assertThat( event.getVisitorTeam() ).isEqualTo( eventEntity.getVisitorTeam() );
		assertThat( event.getMatchDate() ).isEqualTo( eventEntity.getMatchDate() );
		assertThat( event.getMatchDay() ).isEqualTo( eventEntity.getMatchDay() );
		assertThat( event.getPlayCategory() ).isEqualTo( eventEntity.getPlayCategory() );
		assertThat( event.getDivision() ).isEqualTo( eventEntity.getDivision() );
		assertThat( event.getTotalPlayers() ).isEqualTo( eventEntity.getTotalPlayers() );
		assertThat( event.getTotalPayment() ).isEqualTo( eventEntity.getTotalPayment() );
		assertThat( event.getSeasonId() ).isEqualTo( eventEntity.getSeasonId() );
	}

}
