@echo off
REM Run the app (docker container)
REM command line arguments: APP_ENV: the desired environment
REM (optional) DOCKER_IMAGE_TAG the docker image tag
REM example:    run-app.cmd uat (paiement-bdm-api)

REM Load the environment variables
call load-app-env.cmd %*
if NOT %errorlevel% == 0 (
  echo ERROR : An error occured while loading app environment. Aborting the run.
  pause
  exit /b %errorlevel%
)

call change-dir.cmd
echo Current directory: "%cd%"

REM Check if the port is already occupied
for /f %%i in ('netstat -ano ^| findstr ":%HOST_PORT%"') do set PORTS=%%i
if NOT "%PORTS%" == "" (
  echo ERROR : The port %HOST_PORT% is occupied. Aborting.
  pause
  exit /b 1201
)

REM Start the Docker container
@echo on
call docker run -it -d -p %HOST_PORT%:%CONTAINER_PORT% -e "SPRING_PROFILES_ACTIVE=dockerwindows,%APP_ENV%" %DOCKER_IMAGE_TAG% > C:\data\paiement_bdm\%APP_ENV%\logs\paiement-bdm-api.log
@echo off
if NOT %errorlevel% == 0 (
  echo ERROR : Error while running docker run. Aborting.
  pause
  exit /b 1202
)