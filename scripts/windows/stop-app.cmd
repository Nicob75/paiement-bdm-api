@echo off
REM Stops the app as a Docker image
REM command line arguments: APP_ENV: the desired environment
REM example:    run-app.cmd uat

REM Load the environment variables
call load-app-env.cmd %*
if NOT %errorlevel% == 0 (
  echo ERROR : An error occured while loading app environment. Aborting the stop.
  pause
  exit /b %errorlevel%
)

call change-dir.cmd
echo Current directory: "%cd%"

REM Check if the app is already running (<=> if the corresponding port is used)
for /f %%i in ('docker ps --quiet --filter "publish=%HOST_PORT%"') do set CONTAINER_ID=%%i
if "%CONTAINER_ID%" == "" (
  echo ERROR : No container with published port %HOST_PORT% is running. Aborting the stop.
  pause
  exit /b 1301
)

@echo on
call docker stop %CONTAINER_ID%
@echo off
if NOT %errorlevel% == 0 (
  echo ERROR : Error while running docker stop. Aborting.
  pause
  exit /b 1302
)
