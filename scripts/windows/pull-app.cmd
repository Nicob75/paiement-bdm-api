@echo off
REM Pull Docker image
REM command line arguments: APP_ENV: the desired environment => not used, but required because of DOCKER_IMAGE_TAG argument
REM (optional) DOCKER_IMAGE_TAG the docker image tag
REM example:    pull-app.cmd uat (paiement-bdm-api)

REM Load the environment variables
call load-app-env.cmd %*
if NOT %errorlevel% == 0 (
  echo ERROR : An error occured while loading app environment. Aborting.
  pause
  exit /b %errorlevel%
)

REM Pull the Docker image
@echo on
call docker pull %DOCKER_IMAGE_TAG%
@echo off
if NOT %errorlevel% == 0 (
  echo ERROR : Error while running docker pull. Aborting.
  pause
  exit /b 1203
)
