@echo off
REM Load the app environment
REM This script is not meant to be launched just itself.
REM command line arguments: APP_ENV: the desired environment
REM (optional) DOCKER_IMAGE_TAG the docker image tag
REM example:    load-app-env.cmd uat (paiement-bdm-api)

SET ALLOWED_APP_ENVIRONMENTS=uat,prod
if "%~1" == "" (
  echo ERROR : You must specify an environment. Allowed app environments are : %ALLOWED_APP_ENVIRONMENTS%.
  exit /b 1
)

SET APP_ENV=%1
SET HOST_PORT=-1
SET CONTAINER_PORT=9000
SET DOCKER_IMAGE_TAG=chrisnb3/paiement-bdm-api

IF NOT "%~2"=="" (
    set "DOCKER_IMAGE_TAG=%~2"
)

if %APP_ENV% == uat (SET HOST_PORT=9100)
if %APP_ENV% == prod (SET HOST_PORT=9000)
if %HOST_PORT% == -1 (
  echo ERROR : Unknown app environment : %APP_ENV%. Allowed app environments are : %ALLOWED_APP_ENVIRONMENTS%.
  exit /b 1001
)

echo Environment %APP_ENV% successfuly loaded.