use paiement_bdm;
SET @current_season_foot11 = 2;
SET @current_season_foot7 = 2;

INSERT INTO `app_parameters`
(`app_key`,
 `app_value`)
VALUES
    ('CURRENT_DIVISION_FOOT_11',
     @current_season_foot11),
    ('CURRENT_DIVISION_FOOT_7',
     @current_season_foot7);
