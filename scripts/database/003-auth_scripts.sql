create table roles (id integer not null auto_increment, name varchar(20), primary key (id)) engine=InnoDB;
create table user_roles (user_id bigint not null, role_id integer not null, primary key (user_id, role_id)) engine=InnoDB;
create table users (id bigint not null auto_increment, email varchar(50), password varchar(120), username varchar(20), password_expired bit(1) DEFAULT b'0', `last_login_date` timestamp NULL DEFAULT NULL, primary key (id)) engine=InnoDB;
alter table user_roles add constraint FKh8ciramu9cc9q3qcqiv4ue8a6 foreign key (role_id) references roles (id);
alter table user_roles add constraint FKhfh9dx7w3ubf1co1vdev94g3f foreign key (user_id) references users (id);