-- add TotalKeeper column to events table
ALTER TABLE `events`
ADD COLUMN `TotalKeeper` FLOAT
AFTER `TotalPayment`;

UPDATE events
SET TotalKeeper = 0;

-- add Keeper column to eventplayers table
ALTER TABLE `eventplayers`
ADD COLUMN `Keeper` FLOAT
AFTER `Payment`;

UPDATE eventplayers
SET Keeper = 0;
