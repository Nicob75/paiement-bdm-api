#!/bin/bash
# Stops the app as a Docker image
# command line arguments :
# APP_ENV : the desired environment
# example : stop-app.sh uat

# Load the environment variables
source ./load-app-env.sh $*
if ! [ $? -eq 0 ]; then
  echo "An error occured while loading app environment. Aborting."
  pause
  exit $?
fi

source ./change-dir.sh
echo "Current directory: $(pwd)"

# Check if the app is already running (<=> if the corresponding port is used)
CONTAINER_ID=$(docker ps --quiet --filter "publish=$HOST_PORT")
if [ "$CONTAINER_ID" == "" ]; then
  echo "No container with published port $HOST_PORT is running. Aborting the stop."
  pause
  exit 1301
fi

docker stop $CONTAINER_ID
if ! [ $? -eq 0 ]; then
  echo "Error while running docker run. Aborting."
  pause
  exit 1302
else
  echo "Docker container stoped."
fi
