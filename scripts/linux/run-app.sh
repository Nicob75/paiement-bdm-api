#!/bin/bash
# Run the app (docker container)
# command line arguments :
# APP_ENV : the desired environment
# (optional) DOCKER_IMAGE_TAG the docker image tag
# example : run-app.sh uat (paiement-bdm-api)

# Load the environment variables
source ./load-app-env.sh $*
if ! [ $? -eq 0 ]; then
  echo "ERROR : An error occured while loading app environment. Aborting."
  pause
  exit $?
fi

source ./change-dir.sh
echo "Current directory: $(pwd)"

# Check if the port is already occupied
if ( nc -zv localhost $HOST_PORT >/dev/null 2>&1 ); then
  echo "ERROR : The port $HOST_PORT is occupied. Aborting."
  pause
  exit 1201
fi

# Start the Docker container
HOST_IP=$(ip -o route get to 8.8.8.8 | sed -n 's/.*src \([0-9.]\+\).*/\1/p')
docker run -it -d -p $HOST_PORT:$CONTAINER_PORT -e "SPRING_PROFILES_ACTIVE=$APP_ENV" -e "HOST_IP=$HOST_IP" $DOCKER_IMAGE_TAG &>> /opt/DEV/paiement-bdm/data/$APP_ENV/logs/paiement-bdm-api.log
if ! [ $? -eq 0 ]; then
  echo "ERROR : Error while running docker run. Aborting."
  pause
  exit 1202
else
  echo "Docker container started."
fi
