#!/bin/bash
# Builds the app as a Docker image
# command line arguments :
# APP_ENV : the desired environment => not used, but required because of DOCKER_IMAGE_TAG argument
# (optional) DOCKER_IMAGE_TAG the docker image tag
# example : build-app.sh uat (paiement-bdm-api)

# Load the environment variables
source ./load-app-env.sh $*
if ! [ $? -eq 0 ]; then
  echo "ERROR : An error occured while loading app environment. Aborting."
  pause
  exit $?
fi

source ./change-dir.sh
echo "Current directory: $(pwd)"

# Package the app
mvn clean package -DskipTests
if ! [ $? -eq 0 ]; then
  echo "ERROR : Error while running mvn clean package. Aborting."
  pause
  exit 1111
fi

# Build the Docker image
docker build --tag=$DOCKER_IMAGE_TAG .
if ! [ $? -eq 0 ]; then
  echo "ERROR : Error while running docker build. Aborting."
  pause
  exit 1102
fi
