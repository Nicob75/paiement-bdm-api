2021_10_19 - Script: Transform from Sport easy web table to swagger payload - v2

	##### Format for swagger: #####
(\d{2})\/(\d{2})\/(\d{4}) à (\d{2}):(\d{2})
((\d)+)
?(.*?)
(.*)
(\d)*F? ?- ?(\d)*F?
(.*)
?(Annulé)?

~ =>

(\d{2})\/(\d{2})\/(\d{4}) à (\d{2}):(\d{2})\n((\d)+)\n?(.*?)\n(.*)\n(\d)*F? ?- ?(\d)*F?\n(.*)\n?(Annulé)?


{"division": "2",
"playCategory": "Foot a 7",
"seasonId": 7,
"matchDate": "$3-$2-$1T$4:$5:00.000Z",
"matchDay": "$6",
"homeTeam": "$9",
"homeScore": "$10",
"visitorScore": "$11",
"visitorTeam": "$12"},

{"division": "1",
"playCategory": "Foot a 11",
"seasonId": 7,
"matchDate": "$3-$2-$1T$4:$5:00.000Z",
"matchDay": "$6",
"homeTeam": "$9",
"homeScore": "$10",
"visitorScore": "$11",
"visitorTeam": "$12"},
