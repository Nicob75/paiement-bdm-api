FROM openjdk:17-alpine
COPY target/paiement-bdm-api.jar paiement-bdm-api.jar
ENTRYPOINT ["java","-jar","/paiement-bdm-api.jar"]
