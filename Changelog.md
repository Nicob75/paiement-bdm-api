# Changelog

## 2.1.0

- Added feature controller with possibility to get unread features and mark features as read
